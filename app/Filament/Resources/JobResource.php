<?php

namespace App\Filament\Resources;

use App\Filament\Resources\JobResource\Pages;
use App\Filament\Resources\JobResource\RelationManagers;
use App\Models\Job;
use Filament\Actions\Action;
use Filament\Actions\ActionGroup;
use Filament\Actions\CreateAction;
use Filament\Forms;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\MarkdownEditor;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Infolists\Components\Section as ComponentsSection;
use Filament\Infolists\Components\TextEntry;
use Filament\Infolists\Infolist;
use Filament\Resources\DeliveryNoteResource\Tables\Actions\CommitAction;
use Filament\Resources\JobResource\Tables\Actions\ApplyAction;
use Filament\Resources\JobResource\Tables\Actions\CheckAction;
use Filament\Resources\Resource;
use Filament\Support\Enums\FontWeight;
use Filament\Tables;
use Filament\Tables\Actions\ActionGroup as ActionsActionGroup;
use Filament\Tables\Columns\TextColumn\TextColumnSize;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class JobResource extends Resource
{
    protected static ?string $model = Job::class;

    protected static ?string $navigationIcon = 'heroicon-o-building-office';

    protected static ?string $navigationGroup = 'Profile';

    protected static ?int $navigationSort = 3;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('name'),
                TextInput::make('placement'),
                Select::make('location')
                    ->options([
                        'lbn' => 'PT Lintas Bahari Nusantara',
                        'hts' => 'PT Harapan Teknik Shipyard',
                        'kfi' => 'Kazoku Food Indonesia',
                    ]),
                Select::make('education')
                    ->options([
                        'sr_high_school' => 'Sr. High School',
                        'd1' => 'Diploma Degree / D1',
                        'd2' => 'Diploma Degree / D2',
                        'd3' => 'Diploma Degree / D3',
                        'd4' => 'Diploma Degree / D4',
                        'd4' => 'Diploma Degree / D4',
                        's1' => 'Bachelor Degree / S1',
                        's2' => 'Master Degree / S2',
                        's3' => 'Doctorate Degree / S3',
                    ]),
                Select::make('status')
                    ->options([
                        'parttimer' => 'Part Timer',
                        'temporary' => 'Temporary',
                        'experience' => 'Experience',
                    ]),
                DatePicker::make('valid'),
                MarkdownEditor::make('requirement'),
                MarkdownEditor::make('description'),

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\Layout\Split::make([
                    Tables\Columns\Layout\Stack::make([
                        Tables\Columns\TextColumn::make('name')
                            ->size(TextColumnSize::Large)
                            ->weight(FontWeight::ExtraBold)
                            ->color('primary')
                    ]),

                    Tables\Columns\Layout\Stack::make([
                        Tables\Columns\TextColumn::make('education')
                            ->icon('heroicon-m-academic-cap')
                            ->iconColor('primary'),

                        Tables\Columns\TextColumn::make('location')
                            ->icon('heroicon-m-map-pin')
                            ->iconColor('primary')
                    ])->space(2),
                    Tables\Columns\Layout\Stack::make([
                        Tables\Columns\TextColumn::make('placement')
                            ->icon('heroicon-m-building-office')
                            ->iconColor('primary'),

                        Tables\Columns\TextColumn::make('status')
                            ->icon('heroicon-m-shopping-bag')
                            ->iconColor('primary')
                    ])->space(2),
                    Tables\Columns\Layout\Stack::make([
                        Tables\Columns\BadgeColumn::make('status')
                            ->label(__('senzou::filament/resources/delivery-note.columns.status.label'))
                            ->formatStateUsing(function (Model $record) {
                                return $record->applied()
                                    ? ('Applied')
                                    : ('');
                            })
                            ->colors(function (Model $record) {
                                return [
                                    'secondary',
                                    'success' => function () use ($record): bool {
                                        return $record->applied();
                                    },
                                ];
                            }),
                    ])->space(2),
                ])->from('md'),
            ])

            ->filters([
                // SelectFilter::make('status')
                //     ->options([
                //         'draft' => 'Draft',
                //         'reviewing' => 'Reviewing',
                //         'published' => 'Published',
                //     ])
            ])
            ->actions([
                // ActionsActionGroup::make([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
                Tables\Actions\ViewAction::make(),
                ApplyAction::make(),
                // ])
            ])
            ->bulkActions([
                // Tables\Actions\BulkActionGroup::make([
                //     Tables\Actions\DeleteBulkAction::make(),
                // ]),
            ]);
    }

    public static function infolist(Infolist $infolist): Infolist
    {
        return $infolist
            ->schema([
                ComponentsSection::make('')
                    ->schema([
                        TextEntry::make('job', 'job'),
                        TextEntry::make('requirement'),
                        TextEntry::make('description'),
                    ]),
            ]);
    }



    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListJobs::route('/'),
            'create' => Pages\CreateJob::route('/create'),
            // 'edit' => Pages\EditJob::route('/{record}/edit'),
            // 'view' => Pages\ViewJob::route('/{record}'),

        ];
    }
}

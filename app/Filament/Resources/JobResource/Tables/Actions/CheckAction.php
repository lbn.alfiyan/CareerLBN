<?php

namespace Filament\Resources\JobResource\Tables\Actions;

use App\Filament\Resources\JobResource;
use App\Models\Applicant;
use App\Models\Job;
use App\Models\MyApplication;
use App\Models\MyProfile;
use Filament\Tables\Actions\Action;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class CheckAction extends Action
{
    public function setUp(): void
    {
        $this->color('success');

        $this->icon('heroicon-s-check');

        $this->label('Check');

        // $this->modalHeading(fn (): string => __('senzou::filament/resources/delivery-note.actions.commit.single.modal.heading', ['label' => $this->getModelLabel()]));

        // $this->hidden(function (Model $record) {
        //     return MyApplication::query()
        //         ->where('job_id', $record->id)
        //         ->exists();
        // });

        // $this->authorize(function (Model $record) {
        //     return JobResource::canEdit($record);
        // });

        $this->requiresConfirmation();


        $this->action(function (Model $record) {
            // $record = Job::with('profile')->find('id');

            $record->update([
                'applied_at' => $record->full_name,
            ]);
        });
    }

    public static function getDefaultName(): ?string
    {
        return 'check';
    }
}

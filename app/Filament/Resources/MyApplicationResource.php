<?php

namespace App\Filament\Resources;

use App\Filament\Resources\MyApplicationResource\Pages;
use App\Filament\Resources\MyApplicationResource\RelationManagers;
use App\Models\MyApplication;
use Filament\Forms;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Infolists\Components\Section;
use Filament\Infolists\Components\TextEntry;
use Filament\Infolists\Infolist;
use Filament\Resources\Resource;
use Filament\Support\Enums\FontWeight;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\TextColumn\TextColumnSize;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class MyApplicationResource extends Resource
{
    protected static ?string $model = MyApplication::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $navigationGroup = 'Profile';

    protected static ?int $navigationSort = 2;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('Job.name')
                    ->size(TextColumnSize::Large)
                    ->weight(FontWeight::ExtraBold)
                    ->color('primary'),
                TextColumn::make('Job.placement')
                    ->label('Placement')
                    ->icon('heroicon-m-building-office')
                    ->iconColor('primary'),
                TextColumn::make('Job.location')
                    ->label('Location')
                    ->icon('heroicon-m-map-pin')
                    ->iconColor('primary'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),

            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function infolist(Infolist $infolist): Infolist
    {
        return $infolist
            ->schema([
                Section::make('')
                    ->schema([
                        TextEntry::make('job.job'),
                        TextEntry::make('job.requirement')
                            ->label('Requirement'),
                        TextEntry::make('job.description')
                            ->label('Description'),
                    ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMyApplications::route('/'),
            // 'create' => Pages\CreateMyApplication::route('/create'),
            // 'edit' => Pages\EditMyApplication::route('/{record}/edit'),
        ];
    }
}

<?php

namespace App\Filament\Resources\MyApplicationResource\Pages;

use App\Filament\Resources\MyApplicationResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateMyApplication extends CreateRecord
{
    protected static string $resource = MyApplicationResource::class;
}

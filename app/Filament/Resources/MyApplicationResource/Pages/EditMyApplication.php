<?php

namespace App\Filament\Resources\MyApplicationResource\Pages;

use App\Filament\Resources\MyApplicationResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditMyApplication extends EditRecord
{
    protected static string $resource = MyApplicationResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}

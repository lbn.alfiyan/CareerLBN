<?php

namespace App\Filament\Resources\MyApplicationResource\Pages;

use App\Filament\Resources\MyApplicationResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListMyApplications extends ListRecords
{
    protected static string $resource = MyApplicationResource::class;

    // protected function getHeaderActions(): array
    // {
    //     return [
    //         Actions\CreateAction::make(),
    //     ];
    // }
}

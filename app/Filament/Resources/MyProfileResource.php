<?php

namespace App\Filament\Resources;

use App\Filament\Resources\MyProfileResource\Pages;
use App\Filament\Resources\MyProfileResource\RelationManagers;
use App\Filament\Resources\MyProfileResource\RelationManagers\EmergencyContactRelationManager;
use App\Filament\Resources\MyProfileResource\RelationManagers\FamilyRelationManager;
use App\Filament\Resources\MyProfileResource\RelationManagers\FormalEducationRelationManager;
use App\Filament\Resources\MyProfileResource\RelationManagers\InformalEducationRelationManager;
use App\Filament\Resources\MyProfileResource\RelationManagers\PersonaldataRelationManager;
use App\Filament\Resources\MyProfileResource\RelationManagers\ProfileRelationManager;
use App\Filament\Resources\MyProfileResource\RelationManagers\ReferenceContactRelationManager;
use App\Filament\Resources\MyProfileResource\RelationManagers\WorkExperienceRelationManager;
use App\Filament\Resources\MyProfileResource\Tables\Actions\DownloadMyProfileAction;
use App\Filament\Resources\MyProfileResource\Tables\Actions\PreviewMyProfileAction;
use App\Models\MyProfile;
use Filament\Forms;
use Filament\Forms\Components\Checkbox;
use Filament\Forms\Components\CheckboxList;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\MarkdownEditor;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Wizard;
use Filament\Forms\Form;
use Filament\Infolists\Infolist;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Actions\ActionGroup;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Icetalker\FilamentTableRepeater\Forms\Components\TableRepeater;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Parfaitementweb\FilamentCountryField\Forms\Components\Country;
use Parfaitementweb\FilamentCountryField\Tables\Columns\CountryColumn;

class MyProfileResource extends Resource
{
    protected static ?string $model = MyProfile::class;

    protected static ?string $navigationIcon = 'heroicon-o-user-circle';

    protected static ?string $navigationGroup = 'Profile';

    protected static ?int $navigationSort = 1;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Wizard::make([
                    Wizard\Step::make('Personal Details')
                        ->schema([
                            Forms\Components\Section::make('personal_data')
                                ->heading('Personal Data')
                                ->schema([
                                    Forms\Components\Group::make([
                                        SpatieMediaLibraryFileUpload::make('foto_profile'),
                                        TextInput::make('full_name'),
                                        TextInput::make('email_address')->email(),
                                        TextInput::make('id_card')->tel(),
                                        Select::make('gender')
                                            ->options([
                                                'male' => 'Male',
                                                'female' => 'Female'
                                            ]),
                                        Select::make('marital_status')
                                            ->options([
                                                'single' => 'Single',
                                                'married' => 'Married',
                                                'widow' => 'Widow',
                                                'widower' => 'Widower',
                                            ]),
                                        Select::make('religion')
                                            ->options([
                                                'moslem' => 'Moslem',
                                                'christian' => 'Christian',
                                                'catholic' => 'Catholic',
                                                'buddhist' => 'Buddhist',
                                                'konghucu' => 'Kong Hu Cu',
                                            ]),
                                        TextInput::make('place_of_birth'),
                                        DatePicker::make('date_of_birth')->date('d F Y')
                                            ->native(false),
                                        TextInput::make('phone')->tel(),
                                        SpatieMediaLibraryFileUpload::make('cv')->label('CV'),

                                        Forms\Components\Section::make('sim_a')
                                            ->heading('SIM A')
                                            ->schema([
                                                Forms\Components\Group::make([
                                                    TextInput::make('sim_a')->label('ID number')->tel(),
                                                    DatePicker::make('valid_sim_a')->label('Valid to')->native(false),
                                                ])->columns(2)
                                            ]),

                                        Forms\Components\Section::make('sim_b')
                                            ->heading('SIM B')
                                            ->schema([
                                                Forms\Components\Group::make([
                                                    TextInput::make('sim_b')->label('ID number')->tel(),
                                                    DatePicker::make('valid_sim_b')->label('Valid to')->native(false),
                                                ])->columns(2)
                                            ]),

                                        Forms\Components\Section::make('sim_c')
                                            ->heading('SIM C')
                                            ->schema([
                                                Forms\Components\Group::make([
                                                    TextInput::make('sim_c')->label('ID number')->tel(),
                                                    DatePicker::make('valid_sim_c')->label('Valid to')->native(false),
                                                ])->columns(2)
                                            ]),

                                        Forms\Components\Section::make('bpjs')
                                            ->heading('BPJS')
                                            ->schema([
                                                Forms\Components\Group::make([
                                                    TextInput::make('bpjs_kesehatan')->tel(),
                                                    TextInput::make('bpjs_ketenagakerjaan')->tel(),
                                                ])->columns(2)
                                            ]),
                                    ])->columns(3),
                                ])->collapsed(),

                            Forms\Components\Section::make('address')
                                ->heading('Address')
                                ->schema([
                                    Forms\Components\Group::make([
                                        TextInput::make('address'),
                                        TextInput::make('rt')->label('RT')->tel(),
                                        TextInput::make('rw')->label('RW')->tel(),
                                        TextInput::make('kelurahan'),
                                        TextInput::make('kecamatan'),
                                        TextInput::make('city'),
                                        TextInput::make('postal_code')->tel(),
                                        TextInput::make('province_address')->label('Province'),
                                        Country::make('country_address')->label('Country'),

                                    ])->columns(4),
                                ])->collapsed(),

                            Forms\Components\Section::make('salary_expected')
                                ->heading('Salary Expected')
                                ->schema([
                                    Forms\Components\Group::make([
                                        TextInput::make('salary')->tel()->label('Expected Salary'),
                                        TextInput::make('expected_benefit')
                                    ])
                                ])->collapsed(),
                        ]),

                    Wizard\Step::make('Application Question')
                        ->schema([
                            Forms\Components\Section::make('')
                                ->schema([
                                    TextInput::make('question_1')
                                        ->label('Why apply at PT Lintas Bahari Nusantara?')
                                ]),

                            Forms\Components\Section::make('')
                                ->schema([
                                    TextInput::make('question_2')
                                        ->label('Have you applied for any employment in this group/company before? if yes, when? and for what position?')
                                ]),

                            Forms\Components\Section::make('')
                                ->schema([
                                    TextInput::make('question_3')
                                        ->label('Have you ever had any serious diseases/ operations/ accidents?')
                                ]),

                            Forms\Components\Section::make('')
                                ->schema([
                                    TextInput::make('question_4')
                                        ->label('Have you ever officially engaged with Police Department in relation to criminal issue, court of justice or civil cases?')
                                ]),

                            Forms\Components\Section::make('')
                                ->schema([
                                    TextInput::make('question_5')
                                        ->label('Willing to do bussines trip?')
                                ]),

                            Forms\Components\Section::make('')
                                ->schema([
                                    TextInput::make('question_6')
                                        ->label('Willing to be stationed in other city?')
                                ]),

                            Forms\Components\Section::make('')
                                ->schema([
                                    TextInput::make('question_7')
                                        ->label('Willing to work overtime? (Yes/No)')
                                ]),

                            Forms\Components\Section::make('')
                                ->schema([
                                    TextInput::make('question_8')
                                        ->label('Willing to be call? (Yes/No)')
                                ]),

                            Forms\Components\Section::make('')
                                ->schema([
                                    DatePicker::make('question_9')
                                        ->native(false)
                                        ->label('If you are accepted, when you can start working?')
                                ]),
                        ]),
                    // Wizard\Step::make('Information Declaration')
                    //     ->schema([
                    //         Checkbox::make('check')->label('Saya menyatakan bahwa semua informasi di atas adalah benar dan tepat. Saya mengerti bahwa setiap pernyataan yang tidak benar atau terdapat hal yang tidak saya sampaikan, maka cukup untuk dijadikan sebagai pertimbangan untuk dapat diberhentikan dari perusahaan jika saya telah dipekerwjakan'),
                    //         Forms\Components\Group::make([
                    //             SpatieMediaLibraryFileUpload::make('signature'),
                    //             Forms\Components\Group::make([
                    //                 DatePicker::make('date')->default(now())->native(false)
                    //             ])
                    //         ])->columns(2)
                    //     ]),
                ])->columnSpanFull(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('full_name')
                    ->toggleable(isToggledHiddenByDefault: false)
                    ->sortable()
                    ->searchable(),
                TextColumn::make('email_address')
                    ->toggleable(isToggledHiddenByDefault: false)
                    ->sortable()
                    ->searchable(),
                // TextColumn::make('address')
                //     ->toggleable(isToggledHiddenByDefault: true),
                // TextColumn::make('id_card')
                //     ->toggleable(isToggledHiddenByDefault: true)
                //     ->sortable()
                //     ->searchable(),
                // TextColumn::make('phone_number')
                //     ->toggleable(isToggledHiddenByDefault: false)
                //     ->sortable()
                //     ->searchable(),
                // TextColumn::make('place_of_birth')
                //     ->toggleable(isToggledHiddenByDefault: true),
                // TextColumn::make('date_of_birth')
                //     ->date()
                //     ->toggleable(isToggledHiddenByDefault: false),
                // TextColumn::make('gender')
                //     ->toggleable(isToggledHiddenByDefault: false)
                //     ->sortable()
                //     ->searchable(),

                // TextColumn::make('Family.my_profile_id'),
                // TextColumn::make('institution'),
                // TextColumn::make('major'),
                // CountryColumn::make('country_formal'),
                // TextColumn::make('gpa'),
                // TextColumn::make('from')->date('d F Y'),
                // TextColumn::make('to')->date('d F Y'),
            ])

            ->modifyQueryUsing(function (Builder $query) {
                if (auth()->check()) {
                    return $query->where('id', auth()->id());
                }
            })

            ->filters([
                //
            ])
            ->actions([
                ActionGroup::make([
                    PreviewMyProfileAction::make(),
                    DownloadMyProfileAction::make(),
                    Tables\Actions\EditAction::make(),
                    Tables\Actions\DeleteAction::make(),
                ])
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }




    public static function getRelations(): array
    {
        return [
            FormalEducationRelationManager::class,
            InformalEducationRelationManager::class,
            WorkExperienceRelationManager::class,
            FamilyRelationManager::class,
            EmergencyContactRelationManager::class,
            ReferenceContactRelationManager::class,
        ];
    }


    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMyProfiles::route('/'),
            'create' => Pages\CreateMyProfile::route('/create'),
            'edit' => Pages\EditMyProfile::route('/{record}/edit'),
        ];
    }
}

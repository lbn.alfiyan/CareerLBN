<?php

namespace App\Filament\Resources\MyProfileResource\RelationManagers;

use Filament\Forms;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class EmergencyContactRelationManager extends RelationManager
{
    protected static string $relationship = 'EmergencyContact';

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('contact_name'),
                Select::make('relation')
                    ->options([
                        'brother/sister' => 'Brother/Sister',
                        'father' => 'Father',
                        'mother' => 'Mother',
                        'spouse' => 'Spouse',
                    ]),
                TextInput::make('address_emergency'),
                TextInput::make('phone_number_emergency')->label('Phone Number')->tel(),
            ])->columns(4);
    }

    public function table(Table $table): Table
    {
        return $table
            ->recordTitleAttribute('Emergency')
            ->columns([
                Tables\Columns\TextColumn::make('contact_name'),
                Tables\Columns\TextColumn::make('relation'),
                Tables\Columns\TextColumn::make('address_emergency'),
                Tables\Columns\TextColumn::make('phone_number_emergency'),
            ])
            ->filters([
                //
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }
}

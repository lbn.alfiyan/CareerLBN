<?php

namespace App\Filament\Resources\MyProfileResource\RelationManagers;

use Filament\Forms;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class FamilyRelationManager extends RelationManager
{
    protected static string $relationship = 'Family';

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Select::make('relation_family')
                    ->options([
                        'brother/sister' => 'Brother/Sister',
                        'children' => 'Children',
                        'father' => 'Father',
                        'mother' => 'Mother',
                        'spouse' => 'Spouse'
                    ]),
                TextInput::make('child_to'),
                TextInput::make('name_family'),
                Select::make('gender_family')
                    ->options([
                        'male' => 'Male',
                        'female' => 'Female'
                    ]),
                DatePicker::make('date_of_birth_family')->native(false),
            ]);
    }

    public function table(Table $table): Table
    {
        return $table
            ->recordTitleAttribute('name')
            ->columns([
                Tables\Columns\TextColumn::make('relation_family'),
                Tables\Columns\TextColumn::make('child_to'),
                Tables\Columns\TextColumn::make('gender_family'),
                Tables\Columns\TextColumn::make('date_of_birth_family')->date('d F Y'),
            ])
            ->filters([
                //
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }
}

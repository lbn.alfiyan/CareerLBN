<?php

namespace App\Filament\Resources\MyProfileResource\RelationManagers;

use Filament\Forms;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Parfaitementweb\FilamentCountryField\Forms\Components\Country;

class FormalEducationRelationManager extends RelationManager
{
    protected static string $relationship = 'FormalEducation';

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Select::make('level')
                    ->options([
                        'sr_high_school' => 'Sr. High School',
                        'd1' => 'Diploma Degree / D1',
                        'd2' => 'Diploma Degree / D2',
                        'd3' => 'Diploma Degree / D3',
                        'd4' => 'Diploma Degree / D4',
                        'd4' => 'Diploma Degree / D4',
                        's1' => 'Bachelor Degree / S1',
                        's2' => 'Master Degree / S2',
                        's3' => 'Doctorate Degree / S3',
                    ]),
                TextInput::make('institution'),
                TextInput::make('major'),
                Country::make('country_formal'),
                TextInput::make('gpa')
                    ->label('GPA')
                    ->tel(),
                DatePicker::make('from')->native(false),
                DatePicker::make('to')->native(false),
            ]);
    }

    public function table(Table $table): Table
    {
        return $table
            ->recordTitleAttribute('name')
            ->columns([
                Tables\Columns\TextColumn::make('level'),
                Tables\Columns\TextColumn::make('institution'),
                Tables\Columns\TextColumn::make('major'),
                Tables\Columns\TextColumn::make('country_formal'),
                Tables\Columns\TextColumn::make('gpa'),
                Tables\Columns\TextColumn::make('from')->date('d F Y'),
                Tables\Columns\TextColumn::make('to')->date('d F Y'),
            ])
            ->filters([
                //
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }
}

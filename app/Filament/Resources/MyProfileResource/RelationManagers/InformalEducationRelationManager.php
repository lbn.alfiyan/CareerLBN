<?php

namespace App\Filament\Resources\MyProfileResource\RelationManagers;

use Filament\Forms;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Parfaitementweb\FilamentCountryField\Forms\Components\Country;

class InformalEducationRelationManager extends RelationManager
{
    protected static string $relationship = 'InformalEducation';

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('type'),
                TextInput::make('program_name'),
                TextInput::make('organizer'),
                Country::make('country_informal'),
                DatePicker::make('start')->native(false),
                DatePicker::make('end')->native(false),
                SpatieMediaLibraryFileUpload::make('certificate'),
            ]);
    }

    public function table(Table $table): Table
    {
        return $table
            ->recordTitleAttribute('name')
            ->columns([
                Tables\Columns\TextColumn::make('type'),
                Tables\Columns\TextColumn::make('program_name'),
                Tables\Columns\TextColumn::make('organizer'),
                Tables\Columns\TextColumn::make('country_informal'),
                Tables\Columns\TextColumn::make('start')->date('d F Y'),
                Tables\Columns\TextColumn::make('end')->date('d F Y'),
            ])
            ->filters([
                //
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }
}

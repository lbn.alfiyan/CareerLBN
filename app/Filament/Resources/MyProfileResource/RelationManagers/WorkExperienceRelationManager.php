<?php

namespace App\Filament\Resources\MyProfileResource\RelationManagers;

use Filament\Forms;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\MarkdownEditor;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Parfaitementweb\FilamentCountryField\Forms\Components\Country;

class WorkExperienceRelationManager extends RelationManager
{
    protected static string $relationship = 'WorkExperience';

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Group::make([
                    TextInput::make('company_name'),
                    TextInput::make('industry'),
                    TextInput::make('company_address'),
                    TextInput::make('last_position'),
                    Select::make('status')
                        ->options([
                            'part timer' => 'Part Timer',
                            'permanent' => 'Permanent',
                            'temporary' => 'Temporary',
                        ]),
                    DatePicker::make('from_experience')->native(false),
                    DatePicker::make('to_experience')->native(false),
                    TextInput::make('salary')->tel()->label('Last basic salary per Month'),
                    Select::make('nett_gross')
                        ->options([
                            'nett' => 'Nett',
                            'gross' => 'Gross'
                        ]),
                    TextInput::make('office_phone')->tel(),
                ])->columnSpanFull()->columns(3),
                Forms\Components\Group::make([
                    Textarea::make('main_responsibilities'),
                    Textarea::make('reason_for_leaving'),
                ])->columnSpanFull()->columns(2)
            ]);
    }

    public function table(Table $table): Table
    {
        return $table
            ->recordTitleAttribute('name')
            ->columns([
                Tables\Columns\TextColumn::make('company_name'),
                Tables\Columns\TextColumn::make('industry'),
                Tables\Columns\TextColumn::make('company_address'),
                Tables\Columns\TextColumn::make('last_position'),
                Tables\Columns\TextColumn::make('status'),
                Tables\Columns\TextColumn::make('from_experience'),
                Tables\Columns\TextColumn::make('to_experience'),
            ])
            ->filters([
                //
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }
}

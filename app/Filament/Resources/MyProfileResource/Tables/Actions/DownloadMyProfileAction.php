<?php

namespace App\Filament\Resources\MyProfileResource\Tables\Actions;

use Filament\Tables\Actions\Action;
use Illuminate\Database\Eloquent\Model;

class DownloadMyProfileAction extends Action
{
    public function setUp(): void
    {
        $this->icon('heroicon-o-folder-arrow-down');

        $this->color('success');

        $this->label('Download');

        $this->url(function (Model $record) {
            return route('myprofile.download', [
                'record' => $record,
            ]);
        });

        $this->openUrlInNewTab();
    }

    public static function getDefaultName(): ?string
    {
        return 'download';
    }
}

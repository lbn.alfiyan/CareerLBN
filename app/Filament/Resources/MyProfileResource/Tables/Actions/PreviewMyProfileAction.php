<?php

namespace App\Filament\Resources\MyProfileResource\Tables\Actions;

use Filament\Tables\Actions\Action;
use Illuminate\Database\Eloquent\Model;

class PreviewMyProfileAction extends Action
{
    public function setUp(): void
    {
        $this->icon('heroicon-o-document');

        $this->color('primary');

        $this->label('preview');

        $this->url(function (Model $record) {
            return route('myprofile.preview', [
                'record' => $record,
            ]);
        });

        $this->openUrlInNewTab();

        // $this->visible(function (Model $record) {
        //     return $record->isCommitted();
        // });
    }

    public static function getDefaultName(): ?string
    {
        return 'preview';
    }
}

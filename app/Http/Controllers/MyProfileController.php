<?php

namespace App\Http\Controllers;

use App\Models\MyProfile;
use Illuminate\Http\Request;
use Spatie\Browsershot\Browsershot;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;

class MyProfileController extends Controller
{
    // public function preview(int $id)
    // {
    //     $profile = MyProfile::find($id);

    //     return view('preview', [
    //         'profile' => $profile,
    //     ]);
    // }
    public function preview(MyProfile $record)
    {
        return view('preview', [
            'profile' => $record,
        ]);
    }

    public function download(MyProfile $record)
    {

        Browsershot::url(route('myprofile.preview', [$record]))
            ->format('A4')
            ->margins(4, 4, 4, 4)
            ->showBackground()
            ->savePdf($PATH = storage_path(Str::uuid()));

        $fileName = "{$record->full_name}.pdf";

        return Response::download($PATH, $fileName);
    }
}

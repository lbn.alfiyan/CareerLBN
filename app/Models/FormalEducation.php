<?php

namespace App\Models;

use Database\Factories\FormalEducationFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FormalEducation extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'formal_educations';
}

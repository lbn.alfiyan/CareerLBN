<?php

namespace App\Models;

use Database\Factories\JobFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Job extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected static function newFactory()
    {
        return JobFactory::new();
    }

    public function applied(): bool
    {
        return !is_null($this->getAttribute('applied_at'));
    }

    public function job(): BelongsToMany
    {
        return $this->belongsToMany(Applicant::class, 'applicants');
    }

    public function profile(): BelongsTo
    {
        return $this->belongsTo(MyProfile::class, 'id');
    }
}

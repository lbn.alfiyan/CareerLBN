<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class MyApplication extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function Job(): BelongsTo
    {
        return $this->belongsTo(Job::class);
    }
}

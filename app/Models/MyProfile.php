<?php

namespace App\Models;

use Database\Factories\MyProfileFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class MyProfile extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    protected $guarded = [];

    public function FormalEducation(): HasMany
    {
        return $this->hasMany(FormalEducation::class, 'my_profile_id');
    }

    public function InformalEducation(): HasMany
    {
        return $this->hasMany(InformalEducation::class, 'my_profile_id');
    }

    public function WorkExperience(): HasMany
    {
        return $this->hasMany(WorkExperience::class, 'my_profile_id');
    }

    public function Family(): HasMany
    {
        return $this->hasMany(Family::class, 'my_profile_id');
    }

    public function Job(): HasMany
    {
        return $this->hasMany(Job::class, 'my_profile_id');
    }

    public function EmergencyContact(): HasMany
    {
        return $this->hasMany(EmergencyContact::class, 'my_profile_id');
    }

    public function ReferenceContact(): HasMany
    {
        return $this->hasMany(ReferenceContact::class, 'my_profile_id');
    }

    public function profile(): BelongsToMany
    {
        return $this->belongsToMany(Applicant::class, 'applicants');
    }

    protected static function newFactory()
    {
        return MyProfileFactory::new();
    }
}

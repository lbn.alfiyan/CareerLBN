<?php

namespace Database\Factories;

use App\Models\Job;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Job>
 */
class JobFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    protected $model = Job::class;

    public function definition(): array
    {
        $name = fake()->jobTitle();
        $placement = fake()->city();
        $location = fake()->randomElement([
            'lbn' => 'PT Lintas Bahari Nusantara',
            'hts' => 'PT Harapan Teknik Shipyard',
            'kfi' => 'Kazoku Food Indonesia',
        ]);
        $education = fake()->randomElement([
            'sr_high_school' => 'Sr. High School',
            'd1' => 'Diploma Degree / D1',
            'd2' => 'Diploma Degree / D2',
            'd3' => 'Diploma Degree / D3',
            'd4' => 'Diploma Degree / D4',
            'd4' => 'Diploma Degree / D4',
            's1' => 'Bachelor Degree / S1',
            's2' => 'Master Degree / S2',
            's3' => 'Doctorate Degree / S3',
        ]);
        $status = fake()->randomElement([
            'parttimer' => 'Part Timer',
            'temporary' => 'Temporary',
            'experience' => 'Experience',
        ]);
        $valid = fake()->dateTimeAD();
        $requirement = fake()->realText();
        $description = fake()->realText();

        return [
            'name' => $name,
            'placement' => $placement,
            'location' => $location,
            'education' => $education,
            'status' => $status,
            'valid' => $valid,
            'requirement' => $requirement,
            'description' => $description,
        ];
    }
}

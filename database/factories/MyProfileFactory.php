<?php

namespace Database\Factories;

use App\Models\MyProfile;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\MyProfile>
 */
class MyProfileFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    protected $model = MyProfile::class;

    public function definition(): array
    {
        $full_name = fake()->name();
        $email_address = fake()->email();
        $id_card = fake()->randomNumber(5, true);
        $gender = fake()->randomElement([
            'male',
            'female',
        ]);
        $marital_status = fake()->randomElement([
            'single',
            'married',
            'widow',
            'widower'
        ]);
        $religion = fake()->randomElement([
            'moslem',
            'christian',
            'catholic',
            'buddhist'
        ]);
        $place_of_birth = fake()->city();
        $date_of_birth = fake()->dateTimeAD();
        $phone = fake()->phoneNumber();

        $address = fake()->streetAddress();
        $rt = fake()->buildingNumber();
        $rw = fake()->buildingNumber();
        $kelurahan = fake()->cityPrefix();
        $kecamatan = fake()->citySuffix();
        $city = fake()->city();
        $postal_code = fake()->postcode();
        $province_address = fake()->state();
        $country_address = fake()->country();

        $sim_a = fake()->randomNumber(5, true);
        $valid_sim_a = fake()->dateTimeAD();
        $sim_b = fake()->randomNumber(5, true);
        $valid_sim_b = fake()->dateTimeAD();
        $sim_c = fake()->randomNumber(5, true);
        $valid_sim_c = fake()->dateTimeAD();
        $bpjs_kesehatan = fake()->randomNumber(5, true);
        $bpjs_ketenagakerjaan = fake()->randomNumber(5, true);

        $salary = fake()->randomNumber(8, true);
        $expected_benefit = fake()->word(8);

        // $question_1 = fake()->words(5);
        // $question_2 = fake()->words(5);
        // $question_3 = fake()->words(5);
        // $question_4 = fake()->words(5);
        // $question_5 = fake()->words(5);
        // $question_6 = fake()->words(5);

        return [
            'full_name' => $full_name,
            'email_address' => $email_address,
            'id_card' => $id_card,
            'gender' => $gender,
            'marital_status' => $marital_status,
            'religion' => $religion,
            'place_of_birth' => $place_of_birth,
            'date_of_birth' => $date_of_birth,
            'phone' => $phone,

            'address' => $address,
            'rt' => $rt,
            'rw' => $rw,
            'kelurahan' => $kelurahan,
            'kecamatan' => $kecamatan,
            'city' => $city,
            'postal_code' => $postal_code,
            'province_address' => $province_address,
            'country_address' => $country_address,

            'sim_a' => $sim_a,
            'valid_sim_a' => $valid_sim_a,
            'sim_b' => $sim_b,
            'valid_sim_b' => $valid_sim_b,
            'sim_c' => $sim_c,
            'valid_sim_c' => $valid_sim_c,
            'bpjs_kesehatan' => $bpjs_kesehatan,
            'bpjs_ketenagakerjaan' => $bpjs_ketenagakerjaan,

            'salary' => $salary,
            'expected_benefit' => $expected_benefit,

            // 'question_1' => $question_1,
            // 'question_2' => $question_2,
            // 'question_3' => $question_3,
            // 'question_4' => $question_4,
            // 'question_5' => $question_5,
            // 'question_6' => $question_6,
        ];
    }
}

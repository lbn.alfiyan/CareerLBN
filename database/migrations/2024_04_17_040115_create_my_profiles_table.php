<?php

use App\Models\MyProfile;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('my_profiles', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('full_name');
            $table->string('email_address');
            $table->string('id_card');
            $table->string('gender');
            $table->string('marital_status');
            $table->string('religion');
            $table->string('place_of_birth');
            $table->date('date_of_birth');
            $table->string('phone');

            $table->string('address');
            $table->string('rt');
            $table->string('rw');
            $table->string('kelurahan');
            $table->string('kecamatan');
            $table->string('city');
            $table->string('postal_code');
            $table->string('province_address');
            $table->string('country_address');

            $table->string('sim_a')->nullable();
            $table->string('valid_sim_a')->nullable();
            $table->string('sim_b')->nullable();
            $table->string('valid_sim_b')->nullable();
            $table->string('sim_c')->nullable();
            $table->string('valid_sim_c')->nullable();
            $table->string('bpjs_kesehatan')->nullable();
            $table->string('bpjs_ketenagakerjaan')->nullable();

            $table->string('salary');
            $table->string('expected_benefit');

            $table->string('question_1')->nullable();
            $table->string('question_2')->nullable();
            $table->string('question_3')->nullable();
            $table->string('question_4')->nullable();
            $table->string('question_5')->nullable();
            $table->string('question_6')->nullable();
            $table->string('question_7')->nullable();
            $table->string('question_8')->nullable();
            $table->string('question_9')->nullable();

            // $table->string('check');
            // $table->date('date');
        });

        Schema::create('formal_educations', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->foreignId('my_profile_id');
            $table->string('level');
            $table->string('institution');
            $table->string('major');
            $table->string('country_formal');
            $table->string('gpa');
            $table->date('from');
            $table->date('to');
        });

        Schema::create('informal_educations', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->foreignId('my_profile_id');
            $table->string('type')->nullable();
            $table->string('program_name')->nullable();
            $table->string('organizer')->nullable();
            $table->string('country_informal')->nullable();
            $table->date('start')->nullable();
            $table->date('end')->nullable();
        });

        Schema::create('work_experiences', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->foreignId('my_profile_id');
            $table->string('company_name');
            $table->string('industry');
            $table->string('company_address');
            $table->string('last_position');
            $table->string('status');
            $table->string('from_experience');
            $table->string('to_experience');
            $table->string('salary');
            $table->string('nett_gross');
            $table->string('office_phone');
            $table->string('main_responsibilities');
            $table->string('reason_for_leaving');
        });

        Schema::create('families', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->foreignId('my_profile_id');
            $table->string('relation_family');
            $table->string('child_to');
            $table->string('name_family');
            $table->string('gender_family');
            $table->string('date_of_birth_family');
        });

        Schema::create('emergency_contacts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->foreignId('my_profile_id');
            $table->string('contact_name');
            $table->string('relation');
            $table->string('address_emergency');
            $table->string('phone_number_emergency');
        });

        Schema::create('reference_contacts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->foreignId('my_profile_id');
            $table->string('reference_name');
            $table->string('reference_phone');
            $table->string('reference_relation');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('my_profiles');
        Schema::dropIfExists('formal_educations');
        Schema::dropIfExists('informal_educations');
        Schema::dropIfExists('work_experiences');
        Schema::dropIfExists('families');
        Schema::dropIfExists('emergency_contacts');
        Schema::dropIfExists('reference_contacts');
    }
};

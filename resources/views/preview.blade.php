
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  @vite('resources/css/app.css')
</head>
<body>
    <div class="mt-6 mx-3">
      <x-logo class="h-16" />
  <div class="text-5xl font-bold underline my-6 text-center">
    {{ $profile->full_name }}
  </div>
  
  <table class="mb-6 w-full divide-y border text-sm">
    <tbody class="divide-y text-md">
        <tr class="divide-x">
            <td class="w-48 px-2 py-1">
                NIK&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&emsp;&nbsp;&nbsp;&nbsp;: {{ $profile->id_card }}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1">
                Date of Birth&emsp;&emsp;&nbsp;&nbsp;&nbsp;: {{ $profile->date_of_birth }}
                {{-- ->date('d F Y') --}}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1">
                Email&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{ $profile->email_address }}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1">
                Phone Number&emsp;&nbsp;&nbsp;  : {{ $profile->phone }}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1">
                Address&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $profile->address }}, {{ $profile->kelurahan }}, {{ $profile->kecamatan }}, {{ $profile->city }}, {{ $profile->province_address }}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1">
                Expected Salary&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{ $profile->salary }}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1">
                Expected Benefits&nbsp;: {{ $profile->expected_benefit }}
            </td>
        </tr>
    </tbody>
</table>
  
  <table class="w-full divide-y border text-sm">
      <thead>
          <tr class="divide-x text-sm bg-blue-500">
              <th class="py-1 font-semibold">Company Name</th>
              <th class="py-1 font-semibold">Last Position</th>
              <th class="py-1 font-semibold">Status</th>
              <th class="py-1 font-semibold">From</th>
              <th class="py-1 font-semibold">To</th>
              <th class="py-1 font-semibold">Last Salary</th>
              <th class="py-1 font-semibold">Reason For Leaving</th>
            </tr>
        </thead>
        
        <div class="text-2xl text-blue-500 font-semibold">Work Experience</div>
        <tbody class="divide-y">
            @foreach ($profile->WorkExperience as $work)
        <tr class="divide-x">
            <td class="px-2 py-1">
                      <div>
                          {{ $work->company_name }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $work->last_position }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $work->status }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $work->from_experience }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $work->to_experience }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $work->salary }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $work->reason_for_leaving }}
                      </div>
                  </td>
              </tr>
              @endforeach
      </tbody>
  </table>

  <table class="w-full divide-y border text-sm">
      <thead>
          <tr class="divide-x text-sm bg-blue-500">
              <th class="py-1 font-semibold">Level</th>
              <th class="py-1 font-semibold">Institution</th>
              <th class="py-1 font-semibold">Major</th>
              <th class="py-1 font-semibold">GPA</th>
              <th class="py-1 font-semibold">From</th>
              <th class="py-1 font-semibold">To</th>
            </tr>
        </thead>
        
        <div class="text-2xl text-blue-500 font-semibold mt-5">Formal Education</div>
        <tbody class="divide-y">
            @foreach ($profile->FormalEducation as $formal)
        <tr class="divide-x">
            <td class="px-2 py-1">
                      <div>
                          {{ $formal->level }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $formal->institution }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $formal->major }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $formal->gpa }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $formal->from }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $formal->to }}
                      </div>
                  </td>
              </tr>
              @endforeach
      </tbody>
  </table>

  <table class="w-full divide-y border text-sm">
      <thead>
          <tr class="divide-x text-sm bg-blue-500">
              <th class="py-1 font-semibold">Type</th>
              <th class="py-1 font-semibold">Program Name</th>
              <th class="py-1 font-semibold">Organizer</th>
              <th class="py-1 font-semibold">Start</th>
              <th class="py-1 font-semibold">End</th>
            </tr>
        </thead>
        
        <div class="text-2xl text-blue-500 font-semibold mt-5">Informal Education</div>
        <tbody class="divide-y">
            @foreach ($profile->InformalEducation as $informal)
        <tr class="divide-x">
            <td class="px-2 py-1">
                      <div>
                          {{ $informal->type }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $informal->program_name }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $informal->organizer }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $informal->start }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $informal->end }}
                      </div>
                  </td>
              </tr>
              @endforeach
      </tbody>
  </table>

  <table class="w-full divide-y border text-sm">
      <thead>
          <tr class="divide-x text-sm bg-blue-500">
              <th class="py-1 font-semibold">SIM A</th>
              <th class="py-1 font-semibold">Valid SIM A to</th>
              <th class="py-1 font-semibold">SIM B</th>
              <th class="py-1 font-semibold">Valid SIM B</th>
              <th class="py-1 font-semibold">SIM C</th>
              <th class="py-1 font-semibold">Valid SIM C</th>
            </tr>
        </thead>
        
        <div class="text-2xl text-blue-500 font-semibold mt-5">SIM</div>
        <tbody class="divide-y">
        <tr class="divide-x">
            <td class="px-2 py-1">
                      <div>
                          {{ $profile->sim_a }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $profile->valid_sim_a }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $profile->sim_b }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $profile->valid_sim_b }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $profile->sim_c }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $profile->valid_sim_c }}
                      </div>
                  </td>
              </tr>
      </tbody>
  </table>

  <table class="w-full divide-y border text-sm">
      <thead>
          <tr class="divide-x text-sm bg-blue-500">
              <th class="py-1 font-semibold">Emergency Name</th>
              <th class="py-1 font-semibold">Emergency Relation</th>
              <th class="py-1 font-semibold">Emergency Address</th>
              <th class="py-1 font-semibold">Emergency Phone</th>
            </tr>
        </thead>
        
        <div class="text-2xl text-blue-500 font-semibold mt-5">Emergency Contact</div>
        <tbody class="divide-y">
            @foreach ($profile->EmergencyContact as $emergency)
        <tr class="divide-x">
            <td class="px-2 py-1">
                      <div>
                          {{ $emergency->contact_name }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $emergency->relation }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $emergency->address_emergency }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $emergency->phone_number_emergency }}
                      </div>
                  </td>
              </tr>
              @endforeach
      </tbody>
  </table>

  <table class="w-full divide-y border text-sm">
      <thead>
          <tr class="divide-x text-sm bg-blue-500">
              <th class="py-1 font-semibold">Reference Name</th>
              <th class="py-1 font-semibold">Reference Phone</th>
              <th class="py-1 font-semibold">Reference Relation</th>
            </tr>
        </thead>
        
        <div class="text-2xl text-blue-500 font-semibold mt-5">Reference Contact</div>
        <tbody class="divide-y">
            @foreach ($profile->ReferenceContact as $reference)
        <tr class="divide-x">
            <td class="px-2 py-1">
                      <div>
                          {{ $reference->reference_name }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $reference->reference_phone }}
                      </div>
                  </td>
                  <td class="px-2 py-1">
                      <div>
                        {{ $reference->reference_relation }}
                      </div>
                  </td>
              </tr>
              @endforeach
      </tbody>
  </table>

  <div class="my-5 text-2xl text-blue-500 font-semibold">Application Question</div>
  <table class="mb-6 w-full divide-y border border-x-black border-y-black  text-md">
    <tbody class="divide-y">
        <tr class="divide-x border border-black">
            <td class="w-48 px-2 py-1">
               Why apply at PT Lintas Bahari Nusantara? <br> Answer : {{ $profile->question_1 }}
            </td>
        </tr>
        <tr class="divide-x border border-black">
            <td class="w-48 px-2 py-1">
                Have you applied for any employment in this group/company before? if yes, when? and for what position? <br> Answer : {{ $profile->question_2 }}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1 border border-black ">
                Have you ever had any serious diseases/ operations/ accidents? <br> Answer : {{ $profile->question_3 }}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1 border border-black">
                Have you ever officially engaged with Police Department in relation to criminal issue, court of justice or civil cases? <br> Answer : {{ $profile->question_4 }}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1 border border-black">
                Willing to do bussines trip? <br> Answer : {{ $profile->question_5 }}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1 border border-black">
                Willing to be stationed in other city? <br> Answer : {{ $profile->question_6 }}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1 border border-black">
                Willing to work overtime? (Yes/No) <br> Answer : {{ $profile->question_7 }}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1 border border-black">
                Willing to be call? (Yes/No) <br> Answer : {{ $profile->question_8 }}
            </td>
        </tr>
        <tr class="divide-x">
            <td class="w-48 px-2 py-1 border border-black">
                If you are accepted, when you can start working? <br> Answer :  {{ $profile->question_9 }}
            </td>
        </tr>
    </tbody>
</table>

  </div>



</div>



 
</body>
</html>
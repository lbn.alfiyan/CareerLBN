<?php

use App\Http\Controllers\MyProfileController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('preview');
});

Route::get('myprofile/{record}/preview', [MyProfileController::class, 'preview'])
    ->name('myprofile.preview');

Route::get('myprofile/{record}/download', [MyProfileController::class, 'download'])
    ->name('myprofile.download');
